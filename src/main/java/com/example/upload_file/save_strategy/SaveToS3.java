package com.example.upload_file.save_strategy;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.example.upload_file.properties.S3Properties;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Service
@RequiredArgsConstructor
public class SaveToS3 implements SaveStrategyInterface{

    private final S3Properties properties;

    @Override
    public boolean save(String srcFilePath) {
        try {

            log.info("S3 settings:");
            log.info("S3 host:" + properties.getHost());
            log.info("S3 bucket:" + properties.getBucket());

            // Create a minioClient
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint(properties.getHost(), properties.getPort(), properties.isSecure())
                            .credentials(properties.getAccessKey(), properties.getSecretKey())
                            .build();

            // Make bucket if it's not exist.
            boolean found =
                    minioClient.bucketExists(BucketExistsArgs.builder().bucket(properties.getBucket()).build());
            if (!found) {
                // Make a new bucket
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(properties.getBucket()).build());
            }

            File file = new File(srcFilePath);

            log.info("Start uploading file {}", srcFilePath);

            // Upload file as object to bucket
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket(properties.getBucket())
                            .object(file.getName())
                            .filename(file.getAbsolutePath())
                            .build());

            log.info("File successfully uploaded");

        } catch (
                MinioException e) {
            System.out.println("Error occurred: " + e);
            System.out.println("HTTP trace: " + e.httpTrace());
        } catch (IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
