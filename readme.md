
# MinIO S3 server run command example
docker run --name minio --publish 9000:9000 --publish 9001:9001 -e MINIO_ROOT_USER=root -e MINIO_ROOT_PASSWORD=password bitnami/minio:latest


# Kafka
Run from kafka directory

## 1) Run zookeper command
./bin/zookeeper-server-start.sh config/zookeeper.properties

## 2) Run kafka server commnad
./bin/kafka-server-start.sh config/server.properties

## 3) Listen kafka messages in topic
./kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic test

## 4) Show kafka topics
/bin/kafka-topics.sh --bootstrap-server 127.0.0.1:9092 --list 