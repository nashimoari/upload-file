package com.example.upload_file.controller;

import com.example.upload_file.Utils;
import com.example.upload_file.save_strategy.SaveStrategyInterface;
import com.example.upload_file.save_strategy.SaveToKafka;
import com.example.upload_file.save_strategy.SaveToLocalDir;
import com.example.upload_file.save_strategy.SaveToS3;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Controller
@RequiredArgsConstructor
public class FileUploadController {

    @Autowired
    private HttpServletRequest request;

    @Value("${upload-strategy.name}")
    private String saveStrategy;

    private final SaveToLocalDir saveToLocalDir;

    private final SaveToS3 saveToS3;

    private final SaveToKafka saveToKafka;

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public String submit(@RequestParam("file") MultipartFile file, ModelMap modelMap) throws Exception {
        modelMap.addAttribute("file", file);

        if (file.isEmpty()) {
            // file is absent
            Utils utils = new Utils();
            InputStream outputFileStream = utils.getFileFromResourceAsStream("static/index.html");
            return new String(outputFileStream.readAllBytes(), StandardCharsets.UTF_8);
        }

        // Save file to /tmp
        String orgName = file.getOriginalFilename();
        String filePath = "/tmp/" + orgName;
        File tmpDest = new File(filePath);
        file.transferTo(tmpDest);

        SaveStrategyInterface currentStrategy;

        switch (saveStrategy) {
            case "LocalDir":
                currentStrategy = saveToLocalDir;
                break;
            case "S3":
                currentStrategy = saveToS3;
                break;
            case "Kafka":
                currentStrategy = saveToKafka;
                break;

            default:
               throw new IllegalArgumentException("Unkown strategy for saving file");
        }

        currentStrategy.save(filePath);

        // delete file from /tmp/
        if (!tmpDest.delete()) {
            throw new Exception("Failed to delete file " + filePath);
        }

        Utils utils = new Utils();
        InputStream outputFileStream = utils.getFileFromResourceAsStream("static/index.html");
        return new String(outputFileStream.readAllBytes(), StandardCharsets.UTF_8);
    }
}
