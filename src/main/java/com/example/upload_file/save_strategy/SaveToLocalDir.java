package com.example.upload_file.save_strategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Service
public class SaveToLocalDir implements SaveStrategyInterface {

    @Value("${upload-strategy.LocalDir.path}")
    private String uploadFilePath;

    @Override
    public boolean save(String srcFilePath) {

        log.info(srcFilePath);

        if (!new File(uploadFilePath).exists()) {
            new File(uploadFilePath).mkdir();
        }

        Path src = Paths.get(srcFilePath);
        Path dest = Paths.get(uploadFilePath + src.toFile().getName());
        try {
            copy(src.toFile(), dest.toFile());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * This method copy a file from one directory to another. * src is path of the * file to copy dest is the path where to copy the file. * * @param src * @param dest * @throws IOException
     */
    public static void copy(File src, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(src);
            os = new FileOutputStream(dest); // buffer size 1K
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buf)) > 0) {
                os.write(buf, 0, bytesRead);
            }
        } finally {
            is.close();
            os.close();
        }
    }
}
