package com.example.upload_file.save_strategy;

import com.example.upload_file.kafka.MessageProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
@RequiredArgsConstructor
public class SaveToKafka implements SaveStrategyInterface{

    @Autowired
    private final MessageProducer messageProducer;

    @Override
    public boolean save(String srcFilePath) {

        String fileContent = "";

        try {
            byte[] bytes = Files.readAllBytes(Paths.get(srcFilePath));
            fileContent = new String(bytes);
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        messageProducer.sendMessage(fileContent);

        return true;
    }
}
