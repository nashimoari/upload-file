package com.example.upload_file.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Validated
@ConfigurationProperties("upload-strategy.s3")
public class S3Properties {
    @NotEmpty(message = "Parameter 'host' can not be empty")
    private String host;

    @NotNull(message = "Parameter 'port' can not be empty")
    private Integer port;

    @NotEmpty(message = "Parameter 'bucket' can not be empty")
    private String bucket;

    @NotEmpty(message = "Parameter 'access-key' can not be empty")
    private String accessKey;

    @NotEmpty(message = "Parameter 'secret-key' can not be empty")
    private String secretKey;

    public boolean isSecure;
}