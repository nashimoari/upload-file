package com.example.upload_file.save_strategy;

public interface SaveStrategyInterface {
    public boolean save(String srcFilePath);
}
